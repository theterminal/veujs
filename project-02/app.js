const App = {
    data(){
      return {
        placeholderString: 'Input title notes',
            title: 'List notes',
            inputValue: '',
            notes:[]
      }  
    },
    methods:{
        inputChangeHandler(event) {
            this.inputValue = event.target.value
        },
        addNewNote(event) {
            this.notes.push(this.inputValue)
            this.inputValue = ''
        }  
    }
}
// const app = Vue.createApp(App)
// app.mount('#app')

Vue.createApp(App).mount('#app')
const app = {
    data() {
      return {
        courses:[],
        errored:false
      }
    },
    mounted() {
        axios
        .get('https://jsonplaceholder.typicode.com/posts')
        .then(response => this.courses = response.data)
        .catch(error => this.errored = true)
    }
  }

Vue.createApp(app).mount('#app')
  